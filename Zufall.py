#!/usr/bin/python3
import random
from time import *
import os
from classes import standard
from PySide2.QtWidgets import*
from PySide2.QtCore import*
from PySide2.QtGui import*
app = QApplication([])
class MainWindow(QWidget):
	def __init__(self):
		super(MainWindow, self).__init__()
		layout = QBoxLayout(QBoxLayout.TopToBottom, self)

		self.move(standard.window_position)
		self.resize(standard.window_size)

		self.buchstaben_liste_label = QLabel("Liste der zu verwendenden Buchstaben")
		self.buchstaben_liste_edit = QLineEdit(str(standard.buchstaben))
		self.anzahl_zeichen_edit = QLineEdit(str(standard.menge))
		self.anzahl_zeichen_label = QLabel("Anzahl Zeichen")
		self.anzahl_zeichen_checkbox = QCheckBox("Zeichenlimit setzen")
		self.wort_suche_edit = QLineEdit(str(standard.wort))
		self.wort_suche_label = QLabel("Wort Suche")
		self.buchstaben_speichern_checkbox = QCheckBox("Buchstaben in Datei speichern")
		self.buchstaben_speichern_checkbox.setChecked(standard.buchstaben_speichern)
		self.done_button = QPushButton("Starten")
		self.beschleunigen_checkbox = QCheckBox("Vorgang Beschleunigen")
		self.stop_button = QPushButton("Stoppen")
		self.ergebnis_label = QLabel()

		layout.addWidget(self.buchstaben_liste_label)
		layout.addWidget(self.buchstaben_liste_edit)
		layout.addWidget(self.anzahl_zeichen_label)
		layout.addWidget(self.anzahl_zeichen_checkbox)
		layout.addWidget(self.anzahl_zeichen_edit)
		layout.addWidget(self.wort_suche_label)
		layout.addWidget(self.wort_suche_edit)
		layout.addWidget(self.buchstaben_speichern_checkbox)
		layout.addWidget(self.beschleunigen_checkbox)
		layout.addWidget(self.done_button)
		layout.addWidget(self.ergebnis_label)
		layout.addWidget(self.stop_button)

		self.anzahl_zeichen_checkbox.setChecked(True)
		self.ergebnis_label.hide()
		self.stop_button.setEnabled(False)

		self.done_button.clicked.connect(self.done_button_clicked)
		self.stop_button.clicked.connect(self.stop_button_clicked)
		self.anzahl_zeichen_checkbox.stateChanged.connect(self.anzahl_zeichen_checkbox_changed)
		self.beschleunigen_checkbox.stateChanged.connect(self.beschleunigen_checkbox_changed)

	def done_button_clicked(self):
		self.anzahl_zeichen_checkbox.setEnabled(False)
		self.anzahl_zeichen_edit.setEnabled(False)
		self.wort_suche_edit.setEnabled(False)
		self.buchstaben_speichern_checkbox.setEnabled(False)
		self.done_button.setEnabled(False)
		self.stop_button.setEnabled(True)
		self.buchstaben_liste_edit.setEnabled(False)
		self.ergebnis_label.show()
		self.ergebnis_label.setText("Läuft...")

		if self.beschleunigen_checkbox.isChecked() == False:
			self.zeichen_machen_normal()
		else:
			self.zeichen_machen_beschleunigt()
	def stop_button_clicked(self):
		self.anzahl_zeichen_checkbox.setEnabled(True)
		self.anzahl_zeichen_edit.setEnabled(True)
		self.wort_suche_edit.setEnabled(True)
		self.buchstaben_speichern_checkbox.setEnabled(True)
		self.done_button.setEnabled(True)
		self.stop_button.setEnabled(False)
		self.buchstaben_liste_edit.setEnabled(True)
		self.anzahl_zeichen_checkbox_changed()
		self.beschleunigen_checkbox.setEnabled(True)

	def anzahl_zeichen_checkbox_changed(self):
		if self.anzahl_zeichen_checkbox.isChecked() == True:
			self.anzahl_zeichen_edit.setEnabled(True)
		if self.anzahl_zeichen_checkbox.isChecked() == False:
			self.anzahl_zeichen_edit.setEnabled(False)

	def beschleunigen_checkbox_changed(self):
		if self.beschleunigen_checkbox.isChecked():
			self.buchstaben_speichern_checkbox.setEnabled(False)
			self.buchstaben_speichern_checkbox.setChecked(False)
		else:
			self.buchstaben_speichern_checkbox.setEnabled(True)
			self.buchstaben_speichern_checkbox.setChecked(standard.buchstaben_speichern)

	def zeichen_machen_normal(self):
		startzeit = int(time())
		print(startzeit)
		wort_gefunden = False
		anzahl_zeichen = 0
		buchstaben = self.buchstaben_liste_edit.text()
		zufaellige_buchstaben = ""
		wort_suche = self.wort_suche_edit.text()

		if self.anzahl_zeichen_checkbox.isChecked():
			while (int(self.anzahl_zeichen_edit.text()) > anzahl_zeichen) and (wort_gefunden == False):
				anzahl_zeichen = anzahl_zeichen + 1
				zufaellige_buchstaben = zufaellige_buchstaben + str(buchstaben[random.randint(0, len(buchstaben)-1)])
				if wort_suche in zufaellige_buchstaben:
					wort_gefunden = True
		if self.anzahl_zeichen_checkbox.isChecked() == False:
			while wort_gefunden == False:
				anzahl_zeichen = anzahl_zeichen + 1
				zufaellige_buchstaben = zufaellige_buchstaben + str(buchstaben[random.randint(0, len(buchstaben)-1)])
				if wort_suche in zufaellige_buchstaben:
					wort_gefunden = True
				print(anzahl_zeichen)
		endzeit = int(time())
		zeit = endzeit - startzeit
		if wort_gefunden:
			self.ergebnis_label.setText(wort_suche + " gefunden nach " + str(anzahl_zeichen) + " Buchstaben und " + str(zeit) + " Sekunden")
		else:
			self.ergebnis_label.setText("\"" + wort_suche + "\"" + " nicht gefunden nach " + str(anzahl_zeichen) + " Buchstaben und " + str(zeit) + " Sekunden")
		self.stop_button_clicked()
		if self.buchstaben_speichern_checkbox.isChecked:
			enddatei = open("Ergebnisse/" + str(strftime("%y%m%d_%H%M%S")), "w")
			enddatei.write(zufaellige_buchstaben)
			enddatei.close()

	def zeichen_machen_beschleunigt(self):
		startzeit = int(time())
		wort_gefunden = False
		anzahl_zeichen = 0
		buchstaben = self.buchstaben_liste_edit.text()
		zufaellige_buchstaben = ""
		wort_suche = self.wort_suche_edit.text()

		if self.anzahl_zeichen_checkbox.isChecked():
			while (int(self.anzahl_zeichen_edit.text()) > anzahl_zeichen) and (wort_gefunden == False):
				anzahl_zeichen = anzahl_zeichen + 1
				zufaellige_buchstaben = zufaellige_buchstaben + str(buchstaben[random.randint(0, len(buchstaben)-1)])
				if wort_suche in zufaellige_buchstaben:
					wort_gefunden = True
				if anzahl_zeichen > len(wort_suche):
					zufaellige_buchstaben = zufaellige_buchstaben[1:]

		if self.anzahl_zeichen_checkbox.isChecked() == False:
			while wort_gefunden == False:
				anzahl_zeichen = anzahl_zeichen + 1
				zufaellige_buchstaben = zufaellige_buchstaben + str(buchstaben[random.randint(0, len(buchstaben)-1)])
				if wort_suche in zufaellige_buchstaben:
					wort_gefunden = True
				if anzahl_zeichen > len(wort_suche):
					zufaellige_buchstaben = zufaellige_buchstaben[1:]
		endzeit = int(time())
		zeit = endzeit - startzeit
		if wort_gefunden:
			self.ergebnis_label.setText(wort_suche + " gefunden nach " + str(anzahl_zeichen) + " Buchstaben und " + str(zeit) + " Sekunden")
		else:
			self.ergebnis_label.setText("\"" + wort_suche + "\"" + " nicht gefunden nach " + str(anzahl_zeichen) + " Buchstaben und " + str(zeit) + " Sekunden")
		self.stop_button_clicked()

window = MainWindow()
window.show()
app.exec_()

#fobj_out = open("1234", "w")
#fobj_out.write(str(b))
